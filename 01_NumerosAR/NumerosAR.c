/*Autor: Puebla Adlama Diego
Fecha: 13/08/2020
Descripción:Un programa que recibe una cadena "ARAR" compuesta de pares (Arabigo, Romano),
los cuales se calculan sus valores multiplicando el romano por el arabigo. Se suman los
valores de los pares si el Romano del primero es mayor que el romano del segundo y se restan
si el romano del primero es menor que el romano del segundo.
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int ValorRomano (char*);
int ValorArabigo (char*);

int main () {

    char cadena[5];
    int Arabigo1, Arabigo2, Romano1, Romano2, ValorPar1, ValorPar2, Resultado;

    printf("Por favor ingresa una cadena de 4 caracteres.\nDe la forma [Arabigo][Romano][Arabigo][Romano]: ");
    fflush(stdin);
    gets(cadena);
	
    Arabigo1 =ValorArabigo(&cadena[0]);
    Romano1 = ValorRomano(&cadena[1]);
    Arabigo2 =ValorArabigo(&cadena[2]);
    Romano2 = ValorRomano(&cadena[3]);

    if (Romano1 == 0 || Romano2 == 0){

        printf("\nCadena erronea, sigue la notacion.");

    } else {

        ValorPar1 = Romano1 * Arabigo1;
        ValorPar2 = Romano2 * Arabigo2;

        if(Romano1 > Romano2){

            Resultado = ValorPar1 + ValorPar2;

        } else {

            Resultado = ValorPar1 - ValorPar2;

        }

        printf("\nEl resultado es: %d", Resultado);

    }
    
    fflush(stdin);
    getchar();
    return 0;

}

int ValorRomano (char *Romano) {

    switch (*Romano) {
        case 'I':
            return 1;
            break;
        
        case 'V':
            return 5;
            break;

        case 'X':
            return 10;
            break;
        
        case 'L':
            return 50;
            break;

        case 'C':
            return 100;
            break;

        case 'D':
            return 500;
            break;

        case 'M':
            return 1000;
            break;
        default:
            return 0;
            break;
    }

}

int ValorArabigo (char *Arabigo) {

    switch (*Arabigo) {
        case '1':
            return 1;
            break;
        
        case '2':
            return 2;
            break;

        case '3':
            return 3;
            break;
        
        case '4':
            return 4;
            break;

        case '5':
            return 5;
            break;

        case '6':
            return 6;
            break;

        case '7':
            return 7;
            break;
            
        case '8':
            return 8;
            break;
            
        case '9':
            return 9;
            break;
    
    	case '10':
            return 10;
            break;
        default:
            return 0;
            break;
    }

}
